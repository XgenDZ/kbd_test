﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Threading;
using System.Runtime.InteropServices;

namespace ConsoleApp1
{
    static public class SF
    {
        [DllImport("user32.dll")]
        public static extern short GetAsyncKeyState(int vKey);

        [DllImport("user32.dll")]
        public static extern uint keybd_event(byte bVk, byte bScan, int dwFlags, int dwExtraInfo);
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Press K for bScan = 0x2");
            Console.WriteLine("Press L for bScan = 0\n");

            while (true)
            {
                if (SF.GetAsyncKeyState(75) != 0) // K
                {
                    Console.Write("K  ");
                    SF.keybd_event(49, 0x2, 0, 0);
                    Thread.Sleep(50);
                    SF.keybd_event(49, 0x2, 0x2, 0);
                }
                if (SF.GetAsyncKeyState(76) != 0) // L
                {
                    Console.Write("L  ");
                    SF.keybd_event(49, 0, 0, 0);
                    Thread.Sleep(50);
                    SF.keybd_event(49, 0, 0x2, 0);
                }
            }

            Thread.Sleep(500);
        }
    }
}
